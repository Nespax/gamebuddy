
import Foundation
import Combine
import SwiftUI

class UserSession: ObservableObject {
    @Published var isLoggedIn: Bool = false
}
