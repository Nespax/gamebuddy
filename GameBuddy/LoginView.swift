
import Firebase
import FirebaseAuth
import FirebaseFirestore
import SwiftUI

struct LoginView: View {
    
    @State private var email: String = ""
    @State private var password: String = ""
    @EnvironmentObject var userSession: UserSession
    @State private var showErrorAlert = false
    @State private var errorMessage: String = ""
    @State private var isMessageSuccess = false

    func loginUser() {
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            if let error = error {
                self.errorMessage = error.localizedDescription
                self.showErrorAlert = true
                self.isMessageSuccess = false
                return
            }
            self.userSession.isLoggedIn = true
        }
    }
    
    func resetFields() {
        email = ""
        password = ""
    }
    
    func resetPassword() {
        let db = Firestore.firestore()
        db.collection("users").whereField("email", isEqualTo: email).getDocuments { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                if querySnapshot!.documents.count > 0 {
                    Auth.auth().sendPasswordReset(withEmail: self.email) { error in
                        if let error = error {
                            self.errorMessage = error.localizedDescription
                            self.showErrorAlert = true
                            self.isMessageSuccess = false
                        } else {
                            self.errorMessage = "E-mail za resetiranje lozinke poslan."
                            self.showErrorAlert = true
                            self.isMessageSuccess = true
                        }
                    }
                } else {
                    self.errorMessage = "Nema korisnika s tom e-mail adresom."
                    self.showErrorAlert = true
                    self.isMessageSuccess = false
                }
            }
        }
    }



    var body: some View {
        NavigationView {
            VStack(spacing: 20) {
                if userSession.isLoggedIn {
                    GameListView()
                } else {
                    TextField("Email", text: $email)
                        .keyboardType(.emailAddress)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(10)
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))

                    SecureField("Lozinka", text: $password)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(10)
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
                    
                    Text("Zaboravili ste lozinku?")
                        .foregroundColor(.blue)
                        .onTapGesture {
                            self.resetPassword()
                        }
                    
                    
                    Spacer().frame(height: 20)

                    Button(action: {
                        self.loginUser()
                    }) {
                        Text("Prijava")
                            .frame(maxWidth: .infinity)
                            .padding(.vertical, 15)
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                    }
                    
                    HStack {
                        Text("Nemaš račun?")
                            .foregroundColor(Color.gray)
                        NavigationLink(destination: RegistrationView()) {
                            Text("Registriraj se")
                                .foregroundColor(Color.blue)
                        }
                    }
                }
            }
            .padding()
            .onAppear {
                NotificationCenter.default.addObserver(forName: Notification.Name("UserDidLogout"), object: nil, queue: .main) { _ in
                    self.resetFields()
                }
            }
            .alert(isPresented: $showErrorAlert) {
                Alert(
                    title: Text(isMessageSuccess ? "Obavijest" : "Greška"),
                    message: Text(errorMessage),
                    dismissButton: .default(Text("U redu"))
                )
            }
        }
    }
}

