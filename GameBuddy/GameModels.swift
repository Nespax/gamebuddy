
import Foundation

struct RawgGameResponse: Codable {
    var results: [Game]
}

struct Genre: Identifiable, Codable {
    var id: Int
    var name: String
    var slug: String
}

struct Platform: Identifiable, Codable {
    var id: Int
    var name: String
    var slug: String
}

struct PlatformContainer: Codable {
    var platform: Platform
}


struct Game: Identifiable, Codable {
    var id: Int
    var name: String
    var released: String
    var background_image: String?
    var genres: [Genre]?
    var platforms: [PlatformContainer]?
}




