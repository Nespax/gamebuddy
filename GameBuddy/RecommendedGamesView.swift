
import SwiftUI
import Firebase
import FirebaseAuth
import FirebaseFirestore
import URLImage

struct RecommendedGamesView: View {
    @State private var pageNumber: Int = 1
    @State private var recommendedGames: [Game] = []
    @State private var games: [Game] = []

    struct GameRow: View, Equatable {
        var game: Game
        var onDelete: (() -> Void)?
        
        
        func convertDateFormat(dateString: String) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            if let date = dateFormatter.date(from: dateString) {
                dateFormatter.dateFormat = "dd-MM-yyyy"
                return dateFormatter.string(from: date)
            }
            return dateString
        }


        var body: some View {
            ZStack {
                Color.gray.opacity(0.1)
                    .cornerRadius(15)

                VStack(spacing: 0) {
                    if let imageUrl = URL(string: game.background_image ?? "") {
                        URLImage(imageUrl, content: { image in
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .clipped()
                        })
                        .cornerRadius(15)
                        .id(UUID())
                    }

                    VStack(alignment: .center, spacing: 10) {
                        Text(game.name).bold()
                            .font(Font.system(size: 22))
                            .padding(.top, 15)
                            .padding(.bottom, 10)
                            .lineLimit(nil)
                            .fixedSize(horizontal: false, vertical: true)
                            .multilineTextAlignment(.center)

                        VStack(alignment: .center) {
                            Text("Datum izlazka").bold()
                                .font(.subheadline)
                            Text("\(convertDateFormat(dateString: game.released))")
                                .font(.subheadline)
                                .padding(.bottom, 3)

                            Text("Žanr").bold()
                                .font(.subheadline)
                            Text("\(game.genres?.map({ $0.name }).joined(separator: ", ") ?? "Nema žanrova")")
                                .font(.subheadline)
                                .padding(.bottom, 3)

                            Text("Platforme").bold()
                                .font(.subheadline)
                            Text("\(game.platforms?.map({ $0.platform.name }).joined(separator: ", ") ?? "Nema platformi")")
                                .font(.subheadline)
                                .fixedSize(horizontal: false, vertical: true)
                                .multilineTextAlignment(.center)
                                .padding(.bottom, 10)
                        }
                        .padding([.horizontal, .bottom], 10)

                    }
                }
            }
            .frame(maxWidth: .infinity)
            .background(Color.gray.opacity(0.1))
            .cornerRadius(15)
            .shadow(color: Color.black.opacity(0.1), radius: 5, x: 0, y: 5)
        }




        static func == (lhs: GameRow, rhs: GameRow) -> Bool {
            return lhs.game.id == rhs.game.id
        }
    }

    
    
    
    
    func fetchGamesFromFirestore() {
        guard let userId = Auth.auth().currentUser?.uid else {
            print("Nema pristupa trenutnom korisniku.")
            return
        }

        let db = Firestore.firestore()

        db.collection("users").document(userId).collection("games").order(by: "timestamp", descending: true).getDocuments(completion: { (snapshot, error) in
            if let error = error {
                print("Greška prilikom dohvaćanja igara: \(error.localizedDescription)")
                return
            }

            if let documents = snapshot?.documents {
                self.games = documents.compactMap { game(from: $0) }
                recommendGames()
            }
        })
    }

    func game(from document: DocumentSnapshot) -> Game? {
        guard let data = document.data() else { return nil }

        let id = data["id"] as? Int ?? 0
        let name = data["name"] as? String ?? ""
        let released = data["released"] as? String ?? ""
        let background_image = data["background_image"] as? String

        let genresData = data["genres"] as? [[String: Any]]
        let genres = genresData?.compactMap { genreData in
            return Genre(id: genreData["id"] as? Int ?? 0, name: genreData["name"] as? String ?? "", slug: genreData["slug"] as? String ?? "")
        }

        let platformContainersData = data["platforms"] as? [[String: Any]]
        let platforms: [PlatformContainer]? = platformContainersData?.compactMap { platformData -> PlatformContainer? in
            guard let name = platformData["name"] as? String, let id = platformData["id"] as? Int, let slug = platformData["slug"] as? String else { return nil }
            let platform = Platform(id: id, name: name, slug: slug)
            return PlatformContainer(platform: platform)
        }

        return Game(id: id, name: name, released: released, background_image: background_image, genres: genres, platforms: platforms)
    }

    
    func recommendGames() {
        var genreCount: [String: Int] = [:]
        for game in games {
            for genre in game.genres ?? [] {
                genreCount[genre.name] = (genreCount[genre.name] ?? 0) + 1
            }
        }

        let topGenres = Array(genreCount.sorted(by: { $0.value > $1.value }).prefix(3))

        for (genre, _) in topGenres {
            let genreQuery = genre.lowercased().addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            let apiKey = "688f337f3462425aa49b9357fbee3d90"
            let urlString = "https://api.rawg.io/api/games?key=\(apiKey)&genres=\(genreQuery)&page_size=50&page=\(pageNumber)"

            guard let url = URL(string: urlString) else { return }

            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let data = data {
                    let decoder = JSONDecoder()
                    do {
                        let gameResponse = try decoder.decode(RawgGameResponse.self, from: data)
                        DispatchQueue.main.async {
                            let newGames = gameResponse.results.filter { game in
                                return !self.recommendedGames.contains(where: { $0.id == game.id }) && !self.games.contains(where: { $0.id == game.id })
                            }
                            self.recommendedGames.append(contentsOf: newGames.shuffled().prefix(20 - self.recommendedGames.count))
                        }
                    } catch {
                        print("Greška prilikom dekodiranja: \(error)")
                    }
                }
            }.resume()
        }

        pageNumber += 1
    }




    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    Text("Preporučene igre")
                        .font(.largeTitle)
                        .fontWeight(.bold)
                        .padding()
                    
                    Spacer()
                    
                    Button(action: {
                        self.recommendedGames.removeAll()
                        self.fetchGamesFromFirestore()
                        self.recommendGames()
                        self.pageNumber += 1
                    }) {
                        Image(systemName: "arrow.clockwise")
                            .font(.largeTitle)
                            .padding()
                    }
                }
                
                ScrollView {
                    LazyVStack {
                        ForEach(recommendedGames) { game in
                            GameRow(game: game, onDelete: nil)
                                .padding(.bottom)
                        }
                    }
                }
                .padding()
                .shadow(color: Color.black.opacity(0.1), radius: 10, x: 0, y: 10)
            }
            .onAppear {
                fetchGamesFromFirestore()
                recommendGames()
            }
        }
    }

}
