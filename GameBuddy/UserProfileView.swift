
import Firebase
import FirebaseAuth
import FirebaseFirestore

import SwiftUI

struct UserProfileView: View {
    @Binding var isPresented: Bool
    @EnvironmentObject var userSession: UserSession
    @State private var name: String = ""
    @State private var surname: String = ""
    @State private var birthDate = Date()
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var isChangingPassword: Bool = false
    @State private var newPassword: String = ""
    @State private var confirmPassword: String = ""
    @State private var isAlertSuccess: Bool = false
    @State private var showErrorAlert: Bool = false
    @State private var errorMessage: String = ""

    var body: some View {
        if userSession.isLoggedIn {
            VStack {
                Image(systemName: "person.circle.fill")
                     .resizable()
                     .scaledToFit()
                     .frame(width: 100, height: 100)
                     .padding(.bottom, 30)
                     .foregroundColor(.blue)

                Text("Ime: \(name)")
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(Color.white)
                    .cornerRadius(10)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))

                Text("Prezime: \(surname)")
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(Color.white)
                    .cornerRadius(10)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))

                Text("Datum rođenja: \(dateFormatter.string(from: birthDate))")
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(Color.white)
                    .cornerRadius(10)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))

                Text("Email: \(email)")
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(Color.white)
                    .cornerRadius(10)
                    .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))

                if !isChangingPassword {
                    Text("Lozinka: ********")
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(Color.white)
                        .cornerRadius(10)
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
                }

                if isChangingPassword {
                    SecureField("Nova lozinka", text: $newPassword)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(10)
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
                    
                    SecureField("Potvrdi lozinku", text: $confirmPassword)
                        .padding()
                        .background(Color.white)
                        .cornerRadius(10)
                        .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
                }

                HStack(spacing: 20) {
                    Button(action: {
                        if isChangingPassword {
                            changePassword()
                        } else {
                            isChangingPassword.toggle()
                        }
                    }) {
                        Text(isChangingPassword ? "Spremi" : "Promijeni lozinku")
                            .frame(maxWidth: .infinity)
                            .padding()
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                    }

                    Button(action: {
                        do {
                            try Auth.auth().signOut()
                            self.userSession.isLoggedIn = false
                            self.isPresented = false
                            NotificationCenter.default.post(name: Notification.Name("UserDidLogout"), object: nil)
                            
                        } catch let signOutError {
                            errorMessage = "Greška prilikom odjave: \(signOutError)"
                            showErrorAlert = true
                        }
                    }) {
                        Text("Odjava")
                            .frame(maxWidth: .infinity)
                            .padding()
                            .background(Color.red)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                    }
                }
                .padding(.top, 20)

            }
            .padding(.bottom, 150)
            .padding()
            .onAppear(perform: loadUserProfile)
            .alert(isPresented: $showErrorAlert) {
                Alert(title: Text(isAlertSuccess ? "Uspjeh" : "Greška"),
                      message: Text(errorMessage),
                      dismissButton: .default(Text("U redu"))
                )
            }
        } else {
            LoginView()
        }
    }


    private var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return formatter
    }

    func loadUserProfile() {
        let db = Firestore.firestore()
        if let userId = Auth.auth().currentUser?.uid {
            db.collection("users").document(userId).getDocument { (snapshot, error) in
                if let data = snapshot?.data() {
                    self.name = data["name"] as? String ?? ""
                    self.surname = data["surname"] as? String ?? ""
                    self.email = data["email"] as? String ?? ""
                    if let birthDateTimestamp = data["birthdate"] as? Timestamp {
                        self.birthDate = birthDateTimestamp.dateValue()
                    }
                }
            }
        }
    }

    func changePassword() {
        guard newPassword == confirmPassword else {
            errorMessage = "Lozinke se ne podudaraju."
            showErrorAlert = true
            return
        }

        Auth.auth().currentUser?.updatePassword(to: newPassword) { error in
            if let error = error {
                errorMessage = error.localizedDescription
                showErrorAlert = true
            } else {
                errorMessage = "Lozinka je uspješno promijenjena."
                isAlertSuccess = true
                showErrorAlert = true
                
                NotificationCenter.default.post(name: Notification.Name("UserDidLogout"), object: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    do {
                        try Auth.auth().signOut()
                        self.userSession.isLoggedIn = false
                    } catch let signOutError {
                        errorMessage = "Greška prilikom odjave: \(signOutError)"
                        isAlertSuccess = false
                        showErrorAlert = true
                    }
                }
            }
        }
    }
    
}

