
import Firebase
import FirebaseAuth
import FirebaseFirestore
import URLImage
import SwiftUI

struct GameListView: View {
    @State private var gameName: String = ""
    @State private var games: [Game] = []
    @State private var userName: String = ""
    @State private var showUserProfileView: Bool = false

    
    func fetchUserData() {
        guard let userId = Auth.auth().currentUser?.uid else {
            print("Nema pristupa trenutnom korisniku.")
            return
        }

        let db = Firestore.firestore()
        db.collection("users").document(userId).getDocument { (snapshot, error) in
            if let data = snapshot?.data() {
                self.userName = data["name"] as? String ?? ""
            }
        }
    }


    
    
    struct GameRow: View, Equatable {
        var game: Game
        var onDelete: (() -> Void)?
        
        
        func convertDateFormat(dateString: String) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            if let date = dateFormatter.date(from: dateString) {
                dateFormatter.dateFormat = "dd-MM-yyyy"
                return dateFormatter.string(from: date)
            }
            return dateString
        }


        var body: some View {
            ZStack {
                Color.gray.opacity(0.1)
                    .cornerRadius(15)

                VStack(spacing: 0) {
                    if let imageUrl = URL(string: game.background_image ?? "") {
                        URLImage(imageUrl, content: { image in
                            image
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .clipped()
                        })
                        .cornerRadius(15)
                        .id(UUID())
                    }

                    VStack(alignment: .center, spacing: 10) {
                        Text(game.name).bold()
                            .font(Font.system(size: 22))
                            .padding(.top, 15)
                            .padding(.bottom, 10)
                            .lineLimit(nil)
                            .fixedSize(horizontal: false, vertical: true)
                            .multilineTextAlignment(.center)

                        VStack(alignment: .center) {
                            Text("Datum izlaska").bold()
                                .font(.subheadline)
                            Text("\(convertDateFormat(dateString: game.released))")
                                .font(.subheadline)
                                .padding(.bottom, 3)

                            Text("Žanr").bold()
                                .font(.subheadline)
                            Text("\(game.genres?.map({ $0.name }).joined(separator: ", ") ?? "Nema žanrova")")
                                .font(.subheadline)
                                .padding(.bottom, 3)

                            Text("Platforme").bold()
                                .font(.subheadline)
                            Text("\(game.platforms?.map({ $0.platform.name }).joined(separator: ", ") ?? "Nema platformi")")
                                .font(.subheadline)
                                .fixedSize(horizontal: false, vertical: true)
                                .multilineTextAlignment(.center)
                            
                        }
                        .padding([.horizontal, .bottom], 10)

                        Button(action: {
                            onDelete?()
                        }) {
                            Text("Izbriši").bold()
                                .foregroundColor(.white)
                                .padding(.horizontal, 30)
                                .padding(.vertical, 10)
                                .background(Color.red)
                                .cornerRadius(10)
                                .shadow(radius: 3)
                        }
                        .padding(.top, 10)
                        .padding(.bottom, 15)
                    }
                }
            }
            .frame(maxWidth: .infinity)
            .background(Color.gray.opacity(0.1))
            .cornerRadius(15)
            .shadow(color: Color.black.opacity(0.1), radius: 5, x: 0, y: 5)
        }




        static func == (lhs: GameRow, rhs: GameRow) -> Bool {
            return lhs.game.id == rhs.game.id
        }
    }







    
    func searchGame() {
        let baseUrl = "https://api.rawg.io/api/games"
        let apiKey = "688f337f3462425aa49b9357fbee3d90"
        let query = gameName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let urlString = "\(baseUrl)?key=\(apiKey)&search=\(query)&page_size=1"

        gameName = ""
        
        guard let url = URL(string: urlString) else { return }

        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data {
                let decoder = JSONDecoder()
                do {
                    let gameResponse = try decoder.decode(RawgGameResponse.self, from: data)
                    if let game = gameResponse.results.first {
                        print("Dohvaćeni podaci igre: \(game)")

                        DispatchQueue.main.async {
                            if !self.games.contains(where: { $0.id == game.id }) {
                                self.games.insert(game, at: 0)
                            }

                            let db = Firestore.firestore()
                            let timestamp = Date().timeIntervalSince1970
                            let genresData: [[String: Any]] = game.genres?.map { ["id": $0.id, "name": $0.name, "slug": $0.slug] } ?? []
                            let platformsData: [[String: Any]] = game.platforms?.map { ["id": $0.platform.id, "name": $0.platform.name, "slug": $0.platform.slug] } ?? []
                            db.collection("users").document(Auth.auth().currentUser?.uid ?? "").collection("games").document("\(game.id)").setData([
                                "id": game.id,
                                "name": game.name,
                                "released": game.released,
                                "background_image": game.background_image ?? "Standardna slika",
                                "timestamp": timestamp,
                                "genres": genresData,
                                "platforms": platformsData
                            ]) { error in
                                if let error = error {
                                    print("Greška prilikom spremanja igre: \(error.localizedDescription)")
                                } else {
                                    print("Igra uspješno spremljena!")
                                }
                            }
                        }
                    }
                } catch {
                    print("Greška prilikom dekodiranja: \(error)")
                }
            }
        }.resume()
    }


    
    
    func fetchGamesFromFirestore() {
            guard let userId = Auth.auth().currentUser?.uid else {
                print("Nema pristupa trenutnom korisniku.")
                return
            }

            let db = Firestore.firestore()

            db.collection("users").document(userId).collection("games").order(by: "timestamp", descending: true).getDocuments(completion: { (snapshot, error) in
                if let error = error {
                    print("Greška prilikom dohvaćanja igara: \(error.localizedDescription)")
                    return
                }

                if let documents = snapshot?.documents {
                    self.games = documents.compactMap { game(from: $0) }
                }
            })
        }
    

        func game(from document: DocumentSnapshot) -> Game? {
            guard let data = document.data() else { return nil }
            
            let id = data["id"] as? Int ?? 0
            let name = data["name"] as? String ?? ""
            let released = data["released"] as? String ?? ""
            let background_image = data["background_image"] as? String
            
            let genresData = data["genres"] as? [[String: Any]]
            let genres = genresData?.compactMap { genreData in
                return Genre(id: genreData["id"] as? Int ?? 0, name: genreData["name"] as? String ?? "", slug: genreData["slug"] as? String ?? "")
            }
            let platformContainersData = data["platforms"] as? [[String: Any]]
               let platforms: [PlatformContainer]? = platformContainersData?.compactMap { platformData -> PlatformContainer? in
                   guard let name = platformData["name"] as? String, let id = platformData["id"] as? Int, let slug = platformData["slug"] as? String else { return nil }
                   let platform = Platform(id: id, name: name, slug: slug)
                   return PlatformContainer(platform: platform)
               }

            return Game(id: id, name: name, released: released, background_image: background_image, genres: genres, platforms: platforms)
        }
    
    
    func deleteGameFromFirestore(game: Game) {
        guard let userId = Auth.auth().currentUser?.uid else {
            print("Nema pristupa trenutnom korisniku.")
            return
        }

        let db = Firestore.firestore()
        db.collection("users").document(userId).collection("games").document("\(game.id)").delete() { error in
            if let error = error {
                print("Greška prilikom brisanja igre: \(error.localizedDescription)")
            } else {
                print("Igra uspješno obrisana!")
                if let index = self.games.firstIndex(where: { $0.id == game.id }) {
                    self.games.remove(at: index)
                }
            }
        }
    }
    
    
    func deleteGame(at offsets: IndexSet) {
        offsets.forEach { index in
            let game = games[index]
            deleteGameFromFirestore(game: game)
        }
    }

    
    var body: some View {
        NavigationView {
            VStack {
                
                HStack {
                    Button(action: {
                        self.showUserProfileView.toggle()
                    }) {
                        Image(systemName: "person.circle.fill")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 70, height: 70)
                    }
                    .sheet(isPresented: $showUserProfileView) {
                        UserProfileView(isPresented: $showUserProfileView)
                    }

                    Text(userName)
                        .font(.title2)
                        .padding(.leading, 5)
                }
                .padding(.bottom, 35)

                TextField("Unesite ime igre", text: $gameName)
                    .padding(.all, 10)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color.gray, lineWidth: 1)
                    )

                Button(action: {
                    self.searchGame()
                }) {
                    Text("Dodaj igru")
                        .font(.headline)
                        .foregroundColor(.white)
                        .padding(.horizontal, 20)
                        .padding(.vertical, 10)
                        .background(Color.blue)
                        .cornerRadius(10)
                        .shadow(radius: 3)
                        .padding(.bottom, 5)
                }

                ScrollView {
                    LazyVStack {
                        ForEach(games) { game in
                            GameRow(game: game) {
                                deleteGameFromFirestore(game: game)
                            }
                            .padding(.bottom)
                        }
                    }
                }

                NavigationLink(destination: RecommendedGamesView()) {
                    Text("Prikaži preporučene igre")
                }
                .padding(.top, 10)
            }
            .padding()
            .shadow(color: Color.black.opacity(0.1), radius: 10, x: 0, y: 10)
            .onAppear {
                fetchGamesFromFirestore()
                fetchUserData()
            }
        }
    }
}
