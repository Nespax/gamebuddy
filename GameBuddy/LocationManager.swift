
import Foundation
import CoreLocation
import Combine

class LocationManager: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    private var manager = CLLocationManager()
    @Published var userLocation : CLLocationCoordinate2D!
    
    override init() {
        super.init()
        self.manager.delegate = self
    }
    
    func requestAuthorization() {
        self.manager.requestWhenInUseAuthorization()
    }

    func startUpdating() {
        self.manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.userLocation = location.coordinate
        }
    }
}


