
import Firebase
import FirebaseAuth
import FirebaseFirestore
import CoreLocation

import SwiftUI


struct RegistrationView: View {
    @State private var userLocation : CLLocationCoordinate2D!
    @ObservedObject private var locationManager = LocationManager()
    @State private var name: String = ""
    @State private var surname: String = ""
    @State private var birthDate = Date()
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var confirmPassword: String = ""
    @State private var showErrorAlert = false
    @State private var errorMessage: String = ""

    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>


    func registerUser() {
        
        guard !name.isEmpty else {
            self.errorMessage = "Ime je obavezno."
            self.showErrorAlert = true
            return
        }

        guard !surname.isEmpty else {
            self.errorMessage = "Prezime je obavezno."
            self.showErrorAlert = true
            return
        }
        
        
        guard password == confirmPassword else {
            self.errorMessage = "Lozinke se ne podudaraju."
            self.showErrorAlert = true
            return
        }
        
        
        let userLocation = self.locationManager.userLocation ?? CLLocationCoordinate2D(latitude: 0, longitude: 0)
        
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            if let error = error {
                self.errorMessage = (error.localizedDescription)
                self.showErrorAlert = true
                return
            }
            
            guard let user = authResult?.user else { return }
            
            
            let db = Firestore.firestore()
            db.collection("users").document(user.uid).setData([
                "name": self.name,
                "surname": self.surname,
                "birthdate": self.birthDate,
                "email": self.email,
                "location": GeoPoint(latitude: userLocation.latitude, longitude: userLocation.longitude)
            ]) { error in
                if let error = error {
                    self.errorMessage = "Greška prilikom spremanja korisničkih informacija: \(error.localizedDescription)"
                    self.showErrorAlert = true
                } else {
                    print("Korisničke informacije uspješno spremljene!")
                    self.presentationMode.wrappedValue.dismiss()
                }
            }
        }
    }



    var body: some View {
        VStack(spacing: 20) {
            
            Image(systemName: "person.circle.fill")
                .resizable()
                .scaledToFit()
                .frame(width: 100, height: 100)
                .padding(.bottom, 15)
                .foregroundColor(.blue)

            TextField("Ime", text: $name)
                .padding()
                .background(Color.white)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))

            TextField("Prezime", text: $surname)
                .padding()
                .background(Color.white)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))

            DatePicker("Datum rođenja", selection: $birthDate, displayedComponents: .date)
                .padding()
                .background(Color.white)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))

            TextField("Email", text: $email)
                .keyboardType(.emailAddress)
                .padding()
                .background(Color.white)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))

            SecureField("Lozinka", text: $password)
                .padding()
                .background(Color.white)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))
            
            SecureField("Potvrdi lozinku", text: $confirmPassword)
                .padding()
                .background(Color.white)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color.gray, lineWidth: 1))


            Button(action: {
                self.registerUser()
            }) {
                Text("Registriraj se")
                    .frame(maxWidth: .infinity)
                    .padding(.vertical, 15)
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(10)
            }

            HStack {
                Text("Imaš račun?")
                    .foregroundColor(Color.gray)
                NavigationLink(destination: LoginView()) {
                    Text("Prijavi se")
                        .foregroundColor(Color.blue)
                }
            }
        }
        .padding()
        .onAppear {
            self.locationManager.requestAuthorization()
            self.locationManager.startUpdating()
        }
        .alert(isPresented: $showErrorAlert) {
            Alert(title: Text("Greška prilikom registracije"), message: Text(errorMessage), dismissButton: .default(Text("U redu")))
        }
    }
}
