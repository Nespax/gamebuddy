//
//  GameBuddyApp.swift
//  GameBuddy
//
//  Created by Lovro Kumić on 07.08.2023..
//

import SwiftUI
import Firebase

@main
struct GameBuddyApp: App {
    @StateObject private var userSession = UserSession()
    
    init() {
        FirebaseApp.configure()
    }

    var body: some Scene {
        WindowGroup {
            LoginView()
                .environmentObject(userSession)
        }
    }
}


